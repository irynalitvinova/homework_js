// HOMEWORK 7. Show list on page.

/* Відповідь на теоретичне питання.
Document Object Model (DOM) потрібен для того, щоб маніпулювати сторінкою - читати інформацію з HTML, створювати та
 змінювати елементи.
Використовуючи JS ми можемо працювати з DOM вузлами як з об'єктами.*/


function makeList(someArray) {

	const unorderedList = document.createElement('ul');

	someArray.map(item => {
		const listItem = document.createElement('li');
		listItem.innerText = `${item}`;
		unorderedList.appendChild(listItem);
	});

	document.body.prepend(unorderedList);
}

makeList(['Kyiv', 'Lviv', 'Odesa', 'Kharkiv', 'Dnipro', 'Chernivtsi']);

