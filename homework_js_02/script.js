// HOMEWORK 2 LOOPS. Find multiples.

// Відповідь на теоретичне питання.
// Цикли використовуються для того, щоб багато разів повторити одну й ту ж саму частину коду.



let userNum  = prompt('Your number, please');

while (!Number.isInteger(+userNum) || Number.isNaN(+userNum) || userNum === null || userNum === '') {

	userNum = prompt('Your number, please');
}

if (userNum >= 5 ) {
	for (let numFive = 5; numFive <= userNum; numFive++) {
		if (!(numFive % 5)) {                                 // кращий варіант ніж if (numFive % 5 === 0)
			console.log(numFive);
		}
	}
}
else {
	console.log('Sorry, no numbers');
}