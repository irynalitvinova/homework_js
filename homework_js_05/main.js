// HOMEWORK 5. User age and password.

/* Відповідь на теоретичне питання.

  Всі спецсимволи починаються зі зворотнього слеша \ , який називають "символом екранування".
  Він потібний для коректного читання рядка інтерпретатором.
  Тобто, якщо потрібно використати спеціальний символ ( наприклад крапку . ), як звичайний,
  то  додаємо до нього зворотній слеш і записуємо \.  .
  Це й називається екрануванням символу. */



const createNewUser = function() {
	const newUser = {};
	let firstName = prompt('Enter your first name, please.');
	let lastName = prompt('Enter your last name, please.');
	let birthday = prompt('Enter your date of birth dd.mm.yyyy');

	newUser.firstName = firstName;
	newUser.lastName = lastName;
	newUser.birthday = birthday;

	newUser.getAge  = function () {
		let dateNow = new Date();
		let yearNow = dateNow.getFullYear();
		let yearBirthday = birthday.substring(6);
		return yearNow - yearBirthday;
	};

	newUser.getLogin = function () {
		return (this.firstName[0] + this.lastName).toLowerCase();
	};

	newUser.getPassword = function () {
		return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6);
	};
	return newUser;
};
let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getLogin());
console.log(newUser.getPassword());

