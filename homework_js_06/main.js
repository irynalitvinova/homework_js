// HOMEWORK 6. Filter array.

/* Відповідь на теоретичне питання.

ForEach   використовується для того, щоб перебрати масив.

ForEach запускає callback функцію на кожному елементі масиву по черзі.*/




const filterBy = function(someArray, typeofData)  {

	return someArray.filter(elemArray => typeof elemArray !== typeofData)
};

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));



