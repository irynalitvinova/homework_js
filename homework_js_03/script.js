//HOMEWORK 3. Math function

/*  Функція потрібна для багаторазового використання коду.
	  Щоб не повторювати один і той самий код у багатьох місцях.

    За допомогою параметрів (аргументів функції) ми можемо передати всередину функції будь-яку інформацію.*/


let userNum1 = prompt('Enter the first number, please.');

while (userNum1 === null || Number.isNaN(+userNum1 ) || userNum1 === '') {
	userNum1 = prompt('Enter the first number again, please.');
}

let userNum2 = prompt('Enter the second number, please.');

while (userNum2 === null || Number.isNaN(+userNum2 ) || userNum2 === '') {
	userNum2 = prompt('Enter the second number again, please.');
}

let userOperator = prompt('Choose the mathematical operator, please: "+", "-", "*", "/".');
while ((userOperator !== '+') && (userOperator !== '-') && (userOperator !== '/') && (userOperator!== '*')) {
	userOperator = prompt('Choose the correct mathematical operator, please: "+", "-", "*", "/".');
}

function calcTwoNumbers(userNum1, userNum2, userOperator) {
	if (userOperator === '+') {
		return +userNum1 + (+userNum2);
	}
	if (userOperator === '-') {
		return userNum1 - userNum2;
	}
	if (userOperator === '*') {
		return userNum1 * userNum2;
	}

	return userNum1 / userNum2;
}
console.log(calcTwoNumbers());